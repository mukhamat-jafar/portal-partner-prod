create table t_claim_to_expired
(
    id           bigint identity,
    claim_reg_no varchar(100),
    expired_at   datetime2,
    status       int,
    primary key (id)
)
go

create table m_app
(
    id   bigint identity,
    name varchar(100),
    primary key (id)
)
go

create table m_claim_flow
(
    id         bigint identity,
    is_deleted bit,
    name       varchar(100),
    primary key (id)
)
go

create table m_claim_flow_type
(
    id                  bigint identity,
    flow_type           varchar(100),
    is_check_document   bit,
    is_edit_document    bit,
    is_edit_information bit,
    is_flow_exist       bit,
    is_input_amount     bit,
    claim_flow_id       bigint,
    primary key (id),
    constraint FKkb58ebbf7u6u21em6aj23o0ob
        foreign key (claim_flow_id) references m_claim_flow
)
go

create table m_company
(
    id                 bigint identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    code               varchar(15),
    is_deleted         bit,
    name               varchar(100),
    primary key (id),
    constraint UK_m1mkjohrbyursy8030sd2uyka
        unique (code)
)
go

create table m_grup
(
    id                 bigint identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    is_deleted         bit,
    name               varchar(100),
    status             bit not null,
    primary key (id)
)
go

create table m_modul
(
    id                 bigint identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    icon               varchar(50),
    is_add             bit,
    is_deleted         bit,
    is_edit            bit,
    is_parent          bit,
    is_view            bit,
    name               varchar(100),
    parent             bigint,
    uri                varchar(100),
    app_id             bigint,
    claim_status_code  varchar(10),
    primary key (id),
    constraint FK2i8t33frpd2t77b7vxbrclhap
        foreign key (app_id) references m_app,
    constraint FK92my2ifgc5toi7expy4flcag
        foreign key (parent) references m_modul
)
go

create table i_grup_modul
(
    group_id bigint not null,
    modul_id bigint not null,
    primary key (group_id, modul_id),
    constraint FKfwrjpgttaehvurmeq03ykmkhn
        foreign key (modul_id) references m_modul,
    constraint FKqtohroek8pygils23kkqvmn8a
        foreign key (group_id) references m_grup
)
go

create table m_policy_claim_document_bak
(
    id                 int identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    condition          bit,
    is_copy_parent     bit,
    is_deleted         bit,
    status             bit,
    claim_id           varchar(255),
    document_type      bigint
)
go

create table m_source_policy
(
    id   bigint identity,
    name varchar(100),
    code varchar(100),
    primary key (id)
)
go

create table m_company_policy
(
    id                 bigint identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    edits_form_setting int,
    is_deleted         bit,
    name               varchar(20),
    policy_no          varchar(20),
    status             bit not null,
    flow_id            bigint,
    company_id         bigint,
    source_id          bigint,
    primary key (id),
    constraint FK7yfc561tonsm1jlwxnfiqkl1o
        foreign key (source_id) references m_source_policy,
    constraint FKjetrdh1dkkfgb0o4gc5csl75j
        foreign key (company_id) references m_company,
    constraint FKt7ctsfy543hmdpoaa1vmuun3h
        foreign key (flow_id) references m_claim_flow
)
go

create table m_company_claim
(
    id                       bigint identity,
    created_by               varchar(255),
    created_date             datetime2,
    last_modified_by         varchar(255),
    last_modified_date       datetime2,
    claim_type               varchar(100),
    expired_days             int,
    is_deleted               bit,
    is_expired               bit,
    is_need_bank_insured     bit,
    is_need_beneficiary_info bit,
    is_trigger               bit,
    is_used_by_customer      bit,
    policy_id                bigint,
    primary key (id),
    constraint FKcvd24feug7fhjlbc4c72xpsvc
        foreign key (policy_id) references m_company_policy
)
go

create table m_policy_claim_auto
(
    id                 int identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    claim_type         varchar(100),
    is_deleted         bit,
    claim_id           bigint,
    primary key (id),
    constraint FKgxrbxa07ylijmbenhikwtx0f5
        foreign key (claim_id) references m_company_claim
)
go

create table m_policy_claim_document
(
    id                 int identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    condition          bit,
    is_copy_parent     bit default '0',
    is_deleted         bit,
    status             bit,
    claim_id           bigint,
    document_type      varchar(255),
    primary key (id),
    foreign key (claim_id) references m_company_claim
        on update cascade
)
go

create table m_claim_document_file
(
    id          bigint identity,
    filename    varchar(100),
    upload_by   bigint,
    upload_date datetime2,
    doc_id      int,
    primary key (id),
    constraint FK32tpajeuarmsjxoj0umdagvdm
        foreign key (doc_id) references m_policy_claim_document
)
go

alter table m_policy_claim_document
    add constraint DF__m_policy___is_co__48C5B0DD default '0' for is_copy_parent
go

create table m_user_internal
(
    id               bigint identity,
    is_deleted       bit,
    password         varchar(100),
    username         varchar(50),
    view_all_company bit,
    group_id         bigint,
    primary key (id),
    constraint UK_ga4u5yhcanuuae553lfgd2cg7
        unique (username),
    constraint FK58pgyyp0davfy5dl4bhqa8pi6
        foreign key (group_id) references m_grup
)
go

create table i_user_company
(
    user_id    bigint not null,
    company_id bigint not null,
    primary key (user_id, company_id),
    constraint FKkpu4354b3y6nh9jgwxgo1vr8e
        foreign key (company_id) references m_company,
    constraint FKlfwu9j230ioy9bd08oukoyk39
        foreign key (user_id) references m_user_internal
)
go

create table m_user_partner
(
    id                 bigint identity,
    created_by         varchar(255),
    created_date       datetime2,
    last_modified_by   varchar(255),
    last_modified_date datetime2,
    email              varchar(100),
    name               varchar(100),
    password           varchar(100),
    username           varchar(100),
    company_id         bigint,
    group_id           bigint,
    primary key (id),
    constraint UK_ehvmk8jl1ro2qthldt2j1a9r5
        unique (username),
    constraint FK1b9onmrpisopviy6s00deut7c
        foreign key (group_id) references m_grup,
    constraint FKokcn10odbt56sei6bfhyjf1id
        foreign key (company_id) references m_company
)
go

create table t_claim_partner_read
(
    id                bigint identity,
    claim_reg_no      varchar(100),
    claim_status_code varchar(100),
    is_read           bit,
    partner           varchar(100),
    primary key (id)
)
go


