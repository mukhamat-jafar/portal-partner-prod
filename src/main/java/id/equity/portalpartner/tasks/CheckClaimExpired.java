package id.equity.portalpartner.tasks;

import id.equity.portalpartner.dto.portalclaim.ClaimPartnerDto;
import id.equity.portalpartner.dto.portalclaim.UpdatedClaimDto;
import id.equity.portalpartner.model.ClaimToExpired;
import id.equity.portalpartner.model.enums.ClaimToExpiredStatus;
import id.equity.portalpartner.repository.ClaimToExpiredRepository;
import id.equity.portalpartner.service.portalclaim.PortalClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
@EnableAsync
public class CheckClaimExpired {
    @Autowired
    ClaimToExpiredRepository claimToExpiredRepository;

    @Autowired
    PortalClaimService portalClaimService;

    @Async
    @Scheduled(cron = "0 */1 * * * ?")
    public void run() {
        Date currentDate = new Date();
        List<ClaimToExpired> claimToExpiredList = claimToExpiredRepository.findByExpiredAtLessThanAndStatus(currentDate, ClaimToExpiredStatus.Active);

        if (claimToExpiredList.isEmpty()) return;

        claimToExpiredList.forEach(this::updateStatus);
    }

    private void updateStatus(ClaimToExpired x) {
        try {
            List<ClaimPartnerDto> claimPartnerDtos = portalClaimService.getListClaimPartner(new HashMap<>() {{
                put("partner", "11");
                put("claim_reg_no", x.getClaimRegNo());
            }});

            if (!claimPartnerDtos.isEmpty()) {
                if (!claimPartnerDtos.get(0).getClaimStatusCode().equals("SC001")) {
                    ClaimToExpired claimToExpired = claimToExpiredRepository.findByClaimRegNo(x.getClaimRegNo());
                    claimToExpired.setStatus(ClaimToExpiredStatus.Skip);

                    claimToExpiredRepository.save(claimToExpired);

                    return;
                }

                UpdatedClaimDto expired = portalClaimService.expiredClaim(new HashMap<>() {{
                    put("transaction_id", claimPartnerDtos.get(0).getTransactionId());
                    put("transaction_form_id", claimPartnerDtos.get(0).getTransactionFormId());
                }});

                if (null != expired && expired.getStatus().toString().equals("1")) {
                    ClaimToExpired claimToExpired = claimToExpiredRepository.findByClaimRegNo(x.getClaimRegNo());
                    claimToExpired.setStatus(ClaimToExpiredStatus.Expired);

                    claimToExpiredRepository.save(claimToExpired);

                    return;
                }
            }

            return;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
