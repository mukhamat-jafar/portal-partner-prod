package id.equity.portalpartner;

import id.equity.portalpartner.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
@EnableIntegration
@IntegrationComponentScan
@EnableScheduling
public class PortalPartnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalPartnerApplication.class, args);
	}

}
