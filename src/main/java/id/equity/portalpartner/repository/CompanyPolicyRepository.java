package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.model.CompanyPolicy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyPolicyRepository extends PagingAndSortingRepository<CompanyPolicy, Long> {
    Iterable<CompanyPolicy> findCompanyPolicyByNameContainingAndCompanyAndIsDeleted(String name, Company company, Boolean isDeleted);
    Optional<CompanyPolicy> findByPolicyNo(String policyNo);
}
