package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.ClaimFlow;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClaimFlowRepository extends PagingAndSortingRepository<ClaimFlow, Long> {
}
