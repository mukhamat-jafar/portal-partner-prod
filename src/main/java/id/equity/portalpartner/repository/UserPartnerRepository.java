package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.UserPartner;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPartnerRepository extends PagingAndSortingRepository<UserPartner, Long> {
    UserPartner findByUsername(String username);
}
