package id.equity.portalpartner.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "m_claim_document_file")
@NoArgsConstructor
public class ClaimDocumentFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String filename;

    @Column(name = "upload_date")
    private Date uploadDate;

    @Column(name = "upload_by")
    private Long uploadBy;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private PolicyClaimDocument policyClaimDocument;

    public ClaimDocumentFile(String filename, Date uploadDate, Long uploadBy, PolicyClaimDocument policyClaimDocument) {
        this.filename = filename;
        this.uploadDate = uploadDate;
        this.uploadBy = uploadBy;
        this.policyClaimDocument = policyClaimDocument;
    }
}
