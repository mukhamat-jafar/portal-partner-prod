package id.equity.portalpartner.model.enums;

public enum ClaimToExpiredStatus {
    Active,
    Expired,
    Skip,
}
