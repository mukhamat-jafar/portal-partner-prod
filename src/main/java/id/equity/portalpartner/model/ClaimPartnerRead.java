package id.equity.portalpartner.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "t_claim_partner_read")
public class ClaimPartnerRead {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "claim_reg_no", length = 100)
    private String claimRegNo;

    @Column(name = "claim_status_code", length = 100)
    private String claimStatusCode;

    @Column(name = "partner", length = 100)
    private String partner;

    private Boolean isRead;
}
