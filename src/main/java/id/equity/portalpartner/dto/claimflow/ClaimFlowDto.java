package id.equity.portalpartner.dto.claimflow;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimFlowDto {
    private Long id;
    private String name;
    private boolean isDeleted;
}
