package id.equity.portalpartner.dto.company;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyDto {
    private Long id;
    private String code;
    private String name;
}
