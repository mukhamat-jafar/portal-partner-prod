package id.equity.portalpartner.dto.companypolicy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.equity.portalpartner.model.ClaimFlow;
import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.model.SourcePolicy;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyPolicyDto {
    private Long id;
    private String policyNo;
    private String name;
    private Integer editsFormSetting;
    private boolean status;
    private boolean isDeleted;
    private String createdBy;
    @JsonManagedReference
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Company company;
    @JsonManagedReference
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SourcePolicy sourcePolicy;
    @JsonManagedReference
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private ClaimFlow claimFlow;
}
