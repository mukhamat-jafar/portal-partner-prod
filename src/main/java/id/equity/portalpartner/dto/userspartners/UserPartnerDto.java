package id.equity.portalpartner.dto.userspartners;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserPartnerDto {
    private String name;
    private String username;
    private String email;
    private Long groupId;
    private Long companyId;
}
