package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SaveClaimDto {
    @SerializedName("source_app_id")
    private String sourceAppId;
    @SerializedName("transaction_form_id")
    private String transactionFormId;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("claim_reg_no")
    private String claimRegNo;
    @SerializedName("policy_no")
    private String policyNo;
    @SerializedName("event_date")
    private String eventDate;
    @SerializedName("claim_type_code")
    private String claimTypeCode;
    @SerializedName("claim_type_name")
    private String claimTypeName;
    @SerializedName("claim_status_code")
    private String claimStatusCode;
    @SerializedName("claim_status_name")
    private String claimStatusName;
    private String currency;
    @SerializedName("claim_amount")
    private String claimAmount;
    @SerializedName("illness_code")
    private String illnessCode;
    @SerializedName("illness_name")
    private String illnessName;
    @SerializedName("illness_text")
    private String illnessText;
    private String beneficiery;
    @SerializedName("beneficiery_ktp_no")
    private String beneficieryKtpNo;
    @SerializedName("beneficiery_relation_insured_code")
    private String beneficieryRelationInsuredCode;
    @SerializedName("beneficiery_family_card_no")
    private String beneficieryFamilyCardNo;
    private String relationship;
    @SerializedName("relation_insured_code")
    private String relationInsuredCode;
    @SerializedName("partner_family_card_no")
    private String partnerFamilyCardNo;
    @SerializedName("partner_account_name")
    private String partnerAccountName;
    @SerializedName("partner_account_no")
    private String partnerAccountNo;
    @SerializedName("partner_currency")
    private String partnerCurrency;
    @SerializedName("partner_bank")
    private String partnerBank;
    @SerializedName("partner_bank_name")
    private String partnerBankName;
    @SerializedName("partner_bank_text")
    private String partnerBankText;
    @SerializedName("is_insured")
    private String isInsured;
    @SerializedName("claimant_relation_insured_code")
    private String claimantRelationInsuredCode;
    @SerializedName("claimant_relation_insured_name")
    private String claimantRelationInsuredName;
    @SerializedName("claimant_name")
    private String claimantName;
    @SerializedName("claimant_email_address")
    private String claimantEmailAddress;
    @SerializedName("claimant_phone_no")
    private String claimantPhoneNo;
    @SerializedName("created_by")
    private String createdBy;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("modified_by")
    private String modifiedBy;
    @SerializedName("modifed_date")
    private String modifiedDate;
    @SerializedName("is_delete")
    private String isDelete;
    @SerializedName("phone_no")
    private String phoneNo;
}
