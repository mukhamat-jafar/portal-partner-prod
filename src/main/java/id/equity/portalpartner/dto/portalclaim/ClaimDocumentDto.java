package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimDocumentDto {
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("transaction_document_id")
    private Long transactionDocumentId;
    @SerializedName("claim_reg_no")
    private String claimRegNo;
    @SerializedName("partner_policy_no")
    private String partnerPolicyNo;
    @SerializedName("document_type_id")
    private Integer documentTypeId;
    @SerializedName("document_code")
    private String documentCode;
    @SerializedName("document_id")
    private Long documentId;
    @SerializedName("claim_type")
    private String claimType;
    private String description;
    private String remarks;
    private String token;
    @SerializedName("receive_doc_check")
    private Integer receiveDocCheck;
    @SerializedName("created_by")
    private String createdBy;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("modified_by")
    private String modifiedBy;
    @SerializedName("modified_date")
    private String modifiedDate;
    private Boolean condition;
}
