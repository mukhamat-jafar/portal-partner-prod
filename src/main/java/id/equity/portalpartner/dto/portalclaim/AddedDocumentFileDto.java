package id.equity.portalpartner.dto.portalclaim;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddedDocumentFileDto {
    private String result;
    private String message;
}
