package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DocumentDto {
    @SerializedName("document_id")
    private Long documentId;
    @SerializedName("document_code")
    private String documentCode;
    @SerializedName("description")
    private String description;
    private String remarks;
    @SerializedName("is_delete")
    private String isDelete;
    private boolean condition;
}
