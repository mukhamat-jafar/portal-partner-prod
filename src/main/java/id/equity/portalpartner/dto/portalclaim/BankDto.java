package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BankDto {
    @SerializedName("bank_id")
    private Long bankId;
    @SerializedName("bank_code")
    private String bankCode;
    @SerializedName("bank_name")
    private String bankName;
    @SerializedName("status_description")
    private String statusDescription;
    @SerializedName("is_delete")
    private Integer isDelete;
}
