package id.equity.portalpartner.dto.companyclaims;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyClaimDto {
    private Integer id;
    private String claimType;
    private boolean isUsedByCustomer;
    private boolean isExpired;
    private Integer expiredDays;
    private boolean isTrigger;
    private boolean isNeedBeneficiaryInfo;
    private boolean isNeedBankInsured;
    private boolean isDeleted;
}
