package id.equity.portalpartner.dto.policyclaimauto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PolicyClaimAutoDto {
    private Integer id;
    private Integer claimType;
    private boolean isDeleted;
}