package id.equity.portalpartner.dto.application;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ListApplicationDto extends ApplicationDto {
    private Long id;
}
