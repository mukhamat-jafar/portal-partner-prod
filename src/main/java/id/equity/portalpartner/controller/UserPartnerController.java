package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.EntityNotFoundException;
import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.dto.userspartners.CreateUserPartnerDto;
import id.equity.portalpartner.dto.userspartners.UserPartnerDto;
import id.equity.portalpartner.model.UserPartner;
import id.equity.portalpartner.repository.UserPartnerRepository;
import id.equity.portalpartner.service.UserPartnerService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/users-partners")
public class UserPartnerController {
    @Autowired
    private UserPartnerRepository userPartnerRepository;

    @Autowired
    private UserPartnerService userPartnerService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<UserPartnerDto>> find() {
        Iterable<UserPartner> userPartners = userPartnerRepository.findAll();
        Type targetListType = new TypeToken<List<UserPartnerDto>>() {}.getType();

        return ResponseEntity.ok(modelMapper.map(userPartners, targetListType));
    }

    @PostMapping
    public ResponseEntity<UserPartnerDto> create(@Valid @RequestBody CreateUserPartnerDto userPartnerDto) throws EntityNotFoundException {
        try{
            userPartnerService.save(userPartnerDto);
        }
        catch (Exception e){
            throw new EntityNotFoundException(UserPartner.class, e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(userPartnerDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserPartnerDto> get(@PathVariable Long id) throws ResourceNotFoundException {
        UserPartner userPartner = userPartnerRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));

        return ResponseEntity.ok(modelMapper.map(userPartner, UserPartnerDto.class));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<UserPartner> patch(@PathVariable Long id, @Valid @RequestBody UserPartnerDto userPartnerDto) throws ResourceNotFoundException {
        UserPartner up =  userPartnerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserPartner userPartner = modelMapper.map(up, UserPartner.class);

        userPartnerRepository.delete(up);

        return ResponseEntity.status(HttpStatus.OK).body(userPartner);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserPartner> remove(@PathVariable Long id) throws ResourceNotFoundException {
        UserPartner up = userPartnerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserPartner userPartner = modelMapper.map(up, UserPartner.class);

        userPartnerRepository.delete(up);

        return ResponseEntity.status(HttpStatus.OK).body(userPartner);
    }
}
