package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.company.CompanyDto;
import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.repository.CompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/companies")
public class CompanyController {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<Company>>> find() {
        Iterable<Company> companies = companyRepository.findAll();
        Type targetListType = new TypeToken<List<Company>>() {}.getType();
        List<Company> company = modelMapper.map(companies, targetListType);
        BaseResponse<List<Company>> listBaseResponse =
                new BaseResponse<>(true, company, "Company retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Company>> create(@Valid @RequestBody CompanyDto companyDto) {
        Company company = modelMapper.map(companyDto, Company.class);
        Company company1 = companyRepository.save(company);
        BaseResponse<Company> companyBaseResponse =
                new BaseResponse<>(true, company1, "Company created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(companyBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Company>> get(@PathVariable Long id) throws ResourceNotFoundException {
        Company company = companyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Company company1 = modelMapper.map(company, Company.class);
        BaseResponse<Company> companyBaseResponse =
                new BaseResponse<>(true, company1, "Company retrieved successfully");

        return ResponseEntity.ok(companyBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<Company>> patch(@PathVariable Long id, @Valid @RequestBody CompanyDto companyDto) throws ResourceNotFoundException {
        companyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Company company = modelMapper.map(companyDto, Company.class);
        company.setId(id);

        Company company1 = companyRepository.save(company);
        BaseResponse<Company> companyBaseResponse =
                new BaseResponse<>(true, company1, "Company patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(companyBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Company>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        Company company = companyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Company company1 = modelMapper.map(company, Company.class);

        companyRepository.delete(company1);

        BaseResponse<Company> companyBaseResponse =
                new BaseResponse<>(true, company1, "Company deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(companyBaseResponse);
    }
}
