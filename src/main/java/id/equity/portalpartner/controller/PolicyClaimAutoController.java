package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.policyclaimauto.PolicyClaimAutoDto;
import id.equity.portalpartner.model.PolicyClaimAuto;
import id.equity.portalpartner.repository.PolicyClaimAutoRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/policy-claim-auto")
public class PolicyClaimAutoController {
    @Autowired
    private PolicyClaimAutoRepository policyClaimAutoRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<PolicyClaimAutoDto>>> find() {
        Iterable<PolicyClaimAuto> policyClaimAutos = policyClaimAutoRepository.findAll();
        Type targetListType = new TypeToken<List<PolicyClaimAutoDto>>() {}.getType();
        List<PolicyClaimAutoDto> policyClaimAutoDtos = modelMapper.map(policyClaimAutos, targetListType);
        BaseResponse<List<PolicyClaimAutoDto>> listBaseResponse =
                new BaseResponse<>(true, policyClaimAutoDtos, "Policy claim auto retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<PolicyClaimAuto>> create(@Valid @RequestBody PolicyClaimAutoDto policyClaimAutoDto) {
        PolicyClaimAuto policyClaimAutoDto1 = modelMapper.map(policyClaimAutoDto, PolicyClaimAuto.class);
        policyClaimAutoRepository.save(policyClaimAutoDto1);
        BaseResponse<PolicyClaimAuto> policyClaimAutoDtoBaseResponse =
                new BaseResponse<>(true, policyClaimAutoDto1, "Policy claim auto created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(policyClaimAutoDtoBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimAutoDto>> get(@PathVariable Integer id) throws ResourceNotFoundException {
        PolicyClaimAuto policyClaimAuto = policyClaimAutoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimAutoDto policyClaimAutoDto = modelMapper.map(policyClaimAuto, PolicyClaimAutoDto.class);
        BaseResponse<PolicyClaimAutoDto> policyClaimAutoDtoBaseResponse =
                new BaseResponse<>(true, policyClaimAutoDto, "Policy claim auto retrieved successfully");

        return ResponseEntity.ok(policyClaimAutoDtoBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimAuto>> patch(@PathVariable Integer id, @Valid @RequestBody PolicyClaimAutoDto policyClaimAutoDto) throws ResourceNotFoundException {
        policyClaimAutoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimAuto policyClaimAuto = modelMapper.map(policyClaimAutoDto, PolicyClaimAuto.class);
        policyClaimAuto.setId(id);

        policyClaimAutoRepository.save(policyClaimAuto);
        BaseResponse<PolicyClaimAuto> policyClaimAutoBaseResponse =
                new BaseResponse<>(true, policyClaimAuto, "Policy claim auto patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(policyClaimAutoBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimAuto>> remove(@PathVariable Integer id) throws ResourceNotFoundException {
        PolicyClaimAuto app = policyClaimAutoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimAuto policyClaimAuto = modelMapper.map(app, PolicyClaimAuto.class);

        policyClaimAutoRepository.delete(app);
        BaseResponse<PolicyClaimAuto> policyClaimAutoBaseResponse =
                new BaseResponse<>(true, policyClaimAuto, "Policy claim auto deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(policyClaimAutoBaseResponse);
    }
}
