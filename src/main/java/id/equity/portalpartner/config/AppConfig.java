package id.equity.portalpartner.config;

import id.equity.portalpartner.repository.UserPartnerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Autowired
    UserPartnerRepository userPartnerRepository;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public UserPartnerRepository userPartnerRepo() {
        return userPartnerRepository;
    }
}
