package id.equity.portalpartner.aspect;

import id.equity.portalpartner.dto.portalclaim.ClaimPartnerDto;
import id.equity.portalpartner.service.ClaimPartnerReadService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Aspect
@Component
public class ClaimPartnerAspect {
    @Autowired
    ClaimPartnerReadService claimPartnerReadService;

    @Autowired
    ModelMapper modelMapper;

    @Around("execution(* getListClaimPartner(*))")
    public Object updateReadStatusAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        Object result = null;
        Map<String, String> args = (Map<String, String>) proceedingJoinPoint.getArgs()[0];
        String claimRegNo = args.get("claim_reg_no");

        try {
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }

        if (claimRegNo == null) return result;

        List<ClaimPartnerDto> claimPartnerDtos = (List<ClaimPartnerDto>) result;
        ClaimPartnerDto cp = claimPartnerDtos.get(0);

        if (cp == null) return result;

        claimPartnerReadService.read(cp.getClaimRegNo(), cp.getClaimStatusCode(), cp.getPartner());

        return result;
    }
}
