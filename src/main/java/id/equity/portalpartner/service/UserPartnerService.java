package id.equity.portalpartner.service;

import id.equity.portalpartner.dto.userspartners.CreateUserPartnerDto;
import id.equity.portalpartner.model.UserPartner;
import id.equity.portalpartner.repository.UserPartnerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserPartnerService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserPartnerRepository userPartnerRepository;

    @Autowired
    private ModelMapper modelMapper;

    public UserPartner save(CreateUserPartnerDto createUserPartnerDto) {
        UserPartner userPartner = new UserPartner();
        userPartner.setPassword(passwordEncoder.encode(createUserPartnerDto.getPassword()));

        return userPartnerRepository.save(userPartner);
    }

    public UserPartner save(CreateUserPartnerDto createUserPartnerDto, @Nullable Long id) {
        UserPartner userPartner = new UserPartner();
        userPartner.setId(id);
        userPartner.setPassword(passwordEncoder.encode(createUserPartnerDto.getPassword()));

        return save(modelMapper.map(userPartner, CreateUserPartnerDto.class));
    }
}
