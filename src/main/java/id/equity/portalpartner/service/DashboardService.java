package id.equity.portalpartner.service;

import id.equity.portalpartner.dto.dashboard.ClaimStatusCountDto;
import id.equity.portalpartner.dto.portalclaim.ClaimPartnerDto;
import id.equity.portalpartner.model.Module;
import id.equity.portalpartner.repository.ModuleRepository;
import id.equity.portalpartner.service.portalclaim.PortalClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DashboardService {
    @Autowired
    PortalClaimService portalClaimService;

    @Autowired
    ModuleRepository moduleRepository;

    public List<ClaimStatusCountDto> claimStatusCountList(String claimStatusCode, String partner) {
        String[] statusCode;

        if (claimStatusCode != null) {
            statusCode = claimStatusCode.split(",");
        } else {
            Iterable<Module> modules = moduleRepository.findAll();
            statusCode = StreamSupport.stream(modules.spliterator(), false)
                    .filter(x -> x.getClaimStatusCode() != null)
                    .map(Module::getClaimStatusCode)
                    .collect(Collectors.toList()).toArray(value -> new String[value]);
        }

        List<ClaimStatusCountDto> claimStatusCountDtos = Arrays.stream(statusCode).map(x -> {
            Map<String, String> params = new HashMap<>();
            params.put("claim_status_code", x);
            params.put("partner", partner);

            try {
                List<ClaimPartnerDto> claimPartnerDtos = portalClaimService.getListClaimPartner(params);
                int count = claimPartnerDtos != null ? claimPartnerDtos.size() : 0;

                return new ClaimStatusCountDto(x, count);
            } catch (IOException err) {
                return new ClaimStatusCountDto(x, 0);
            }
        }).collect(Collectors.toList());

        return claimStatusCountDtos;
    }
}
