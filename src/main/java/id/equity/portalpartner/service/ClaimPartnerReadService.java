package id.equity.portalpartner.service;

import id.equity.portalpartner.dto.claimpartnerread.ClaimPartnerReadDto;
import id.equity.portalpartner.model.ClaimPartnerRead;
import id.equity.portalpartner.repository.ClaimPartnerReadRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClaimPartnerReadService {
    @Autowired
    ClaimPartnerReadRepository claimPartnerReadRepository;

    @Autowired
    ModelMapper modelMapper;

    public ClaimPartnerReadDto read(String claimRegNo, String claimStatusCode, String partner) {
        ClaimPartnerRead claimPartnerRead = claimPartnerReadRepository
                .findByClaimRegNoAndClaimStatusCodeAndPartnerAndIsRead(claimRegNo, claimStatusCode, partner, true);

        if (claimPartnerRead != null) return new ClaimPartnerReadDto();

        ClaimPartnerRead cpr = new ClaimPartnerRead();
        cpr.setClaimRegNo(claimRegNo);
        cpr.setClaimStatusCode(claimStatusCode);
        cpr.setPartner(partner);
        cpr.setIsRead(true);

        claimPartnerReadRepository.save(cpr);

        ClaimPartnerReadDto claimPartnerReadDto = modelMapper.map(cpr, ClaimPartnerReadDto.class);

        return claimPartnerReadDto;
    }
}
