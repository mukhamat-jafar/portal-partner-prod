package id.equity.portalpartner.service.portalclaim;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Condition {
    public String column;
    public String comparison_operator;
    public String value;
    public String logical_operator;
    public String notation;

    public Condition(String column, String comparison_operator, String value, String logical_operator) {
        this.column = column;
        this.comparison_operator = comparison_operator;
        this.value = value;
        this.logical_operator = logical_operator;
    }

    public Condition(String column, String comparison_operator, String value, String logical_operator, String notation) {
        this.column = column;
        this.comparison_operator = comparison_operator;
        this.value = value;
        this.logical_operator = logical_operator;
        this.notation = notation;
    }
}
