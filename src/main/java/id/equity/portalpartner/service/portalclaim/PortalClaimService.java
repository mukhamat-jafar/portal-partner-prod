package id.equity.portalpartner.service.portalclaim;

import com.google.common.base.CaseFormat;
import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.dto.portalclaim.*;
import id.equity.portalpartner.model.*;
import id.equity.portalpartner.model.enums.ClaimToExpiredStatus;
import id.equity.portalpartner.repository.*;
import id.equity.portalpartner.service.portalclaim.interfaces.PortalClaim;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@Service
@Slf4j
public class PortalClaimService {
    private Retrofit retrofit;
    private PortalClaim portalClaim;

    @Autowired
    private CompanyClaimRepository companyClaimRepository;
    @Autowired
    private PolicyClaimDocumentRepository policyClaimDocumentRepository;

    @Autowired
    private UserPartnerRepository userPartnerRepository;

    @Autowired
    private PolicyClaimAutoRepository policyClaimAutoRepository;

    @Autowired
    private CompanyPolicyRepository companyPolicyRepository;

    @Autowired
    private ClaimToExpiredRepository claimToExpiredRepository;

    @Autowired
    private ClaimPartnerReadRepository claimPartnerReadRepository;

    @Value("${portal.username}")
    private String portalUsername;
    @Value("${portal.password}")
    private String portalPassword;
    @Value("${portal.appcode}")
    private String portalAppCode;

    private static final String BASE_URL = "http://api.elife.co.id/api-claim-portal/index.php/api/service/";

    public PortalClaimService() {
        class NullOnEmptyConverterFactory extends Converter.Factory {

            @Override
            public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
                final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
                return new Converter<ResponseBody, Object>() {
                    @Override
                    public Object convert(ResponseBody body) throws IOException {
                        if (body.contentLength() == 0) return null;
                        return delegate.convert(body);                }
                };
            }
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(new NullOnEmptyConverterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
        portalClaim = retrofit.create(PortalClaim.class);
    }

    private String getAction(Map<String, String> params) {
        if(params.get("source_code").equals("AJK")) {
            return "list_insured_ajk";
        }

        return "list_insured_grp";
    }

    public List<InsuredDto> getListInsured(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        String action = getAction(params);

        params.forEach((c, v) -> {
            if (!c.equals("source_code")) {
                if (c.equals("insured_name")) {
                    conditions.add(new Condition(c, "like", v, "AND", "all"));
                    return;
                }

                conditions.add(new Condition(c, "equal", v, "AND"));
            }
        });

        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
            portalUsername,
            portalPassword,
            portalAppCode,
            action,
            conditions,
            "insured_no"
        );

        try {
            Call<PortalClaimBaseResponse<List<InsuredDto>>> call = portalClaim.getListInsured(request);
            Response<PortalClaimBaseResponse<List<InsuredDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<IllnessDto> getListIllness(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_illness",
                conditions,
                "illness_code"
        );

        try {
            Call<PortalClaimBaseResponse<List<IllnessDto>>> call = portalClaim.getListIllness(request);
            Response<PortalClaimBaseResponse<List<IllnessDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimTypeDto> getListClaimType(Long policyId, boolean isUserByCustomer, Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            if (policyId != null) {
                Iterable<CompanyClaim> companyClaim = companyClaimRepository
                        .findByCompanyPolicyIdAndIsUsedByCustomer(policyId, isUserByCustomer);
                String companyClaimIds = StreamSupport.stream(companyClaim.spliterator(), false)
                        .map(cc -> cc.getClaimType())
                        .collect(Collectors.joining(","));
                String value = companyClaimIds != "" ? companyClaimIds : "0";

                conditions.add(new Condition("claim_type_code", "in", value, "AND"));
                return;
            }

            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_type",
                conditions,
                "claim_type_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimTypeDto>>> call = portalClaim.getListClaimType(request);
            Response<PortalClaimBaseResponse<List<ClaimTypeDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<BankDto> getListBank(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_bank",
                conditions,
                "bank_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<BankDto>>> call = portalClaim.getListBank(request);
            Response<PortalClaimBaseResponse<List<BankDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<RelationInsuredDto> getListRelationInsured(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_relation_insured",
                conditions,
                "relation_insured_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<RelationInsuredDto>>> call = portalClaim.getListRelationInsured(request);
            Response<PortalClaimBaseResponse<List<RelationInsuredDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<CurrencyDto> getListCurrency(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_currency",
                conditions,
                "currency_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<CurrencyDto>>> call = portalClaim.getListCurrency(request);
            Response<PortalClaimBaseResponse<List<CurrencyDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimStatusDto> getListClaimStatus(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_status",
                conditions,
                "claim_status_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimStatusDto>>> call = portalClaim.getListClaimStatus(request);
            Response<PortalClaimBaseResponse<List<ClaimStatusDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimPartnerDto> getListClaimPartner(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            if (c.equals("participant")) {
                conditions.add(new Condition(c, "like", v, "AND", "all"));
                return;
            }

            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_partner",
                conditions,
                "source_app_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimPartnerDto>>> call = portalClaim.getListClaimPartner(request);
            Response<PortalClaimBaseResponse<List<ClaimPartnerDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            List<ClaimPartnerRead> claimPartnerReads = claimPartnerReadRepository
                .findByClaimStatusCodeAndPartnerAndIsRead(response.body().getData().get(0).getClaimStatusCode(), params.get("partner"), true);
            List<ClaimPartnerDto> claimPartnerDtos = response.body().getData().stream()
                .map(x -> {
                    if (x.getIdUserPartner() != null) {
                        UserPartner userPartner = userPartnerRepository.findByUsername(x.getIdUserPartner());

                        if (userPartner != null) {
                            x.setIdUserPartner(userPartner.getName());
                        }
                    }

                    ClaimPartnerRead claimPartnerDto = claimPartnerReads.stream()
                        .filter(y -> y.getClaimRegNo().equals(x.getClaimRegNo()))
                        .findAny()
                        .orElse(null);

                    if (claimPartnerDto != null) {
                        x.setRead(true);
                    }

                    return x;
                }).collect(Collectors.toList());

            return claimPartnerDtos;
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimHistoryProcessDto> getListClaimHistoryProcess(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_history_process",
                conditions,
                "transaction_history_process_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimHistoryProcessDto>>> call = portalClaim.getListClaimHistoryProcess(request);
            Response<PortalClaimBaseResponse<List<ClaimHistoryProcessDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            List<ClaimHistoryProcessDto> claimHistoryProcessDtos = response.body().getData().stream().map(x -> {
                if (x.getUserTypeName() != null && x.getUserTypeName().equals("External")) {
                    UserPartner userPartner = userPartnerRepository.findByUsername(x.getUserPartnerName());

                    x.setUserPartnerName(userPartner != null ? userPartner.getName() : x.getUserTypeName());

                    return x;
                }

                return x;
            }).collect(Collectors.toList());

            return claimHistoryProcessDtos;
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimDocumentFileDto> getListClaimDocumentFile(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_document_files",
                conditions,
                "transaction_document_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimDocumentFileDto>>> call = portalClaim.getListClaimDocumentFile(request);
            Response<PortalClaimBaseResponse<List<ClaimDocumentFileDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<ClaimDocumentDto> getListClaimDocument(Map<String, String> params) throws IOException, ResourceNotFoundException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            if (c.equals("document_id") && v != null) {
                conditions.add(new Condition("document_id", "in", v, "AND"));
                return;
            }

            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_claim_document",
                conditions,
                ""
        );

        try {
            Call<PortalClaimBaseResponse<List<ClaimDocumentDto>>> call = portalClaim.getListClaimDocument(request);
            Response<PortalClaimBaseResponse<List<ClaimDocumentDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null || response.body().getData().isEmpty()) {
                return Arrays.asList();
            }

            String policyNo = response.body().getData().get(0).getPartnerPolicyNo();
            String claimType = response.body().getData().get(0).getClaimType();

            CompanyClaim companyClaim = companyClaimRepository.findByCompanyPolicyPolicyNoAndClaimType(policyNo, claimType)
                    .orElseThrow(() -> new ResourceNotFoundException("Company Claim not found"));
            List<PolicyClaimDocument> policyClaimDocuments = policyClaimDocumentRepository.findByCompanyClaimId(companyClaim.getId());

            List<ClaimDocumentDto> claimDocumentDtos = response.body().getData().stream()
                    .map(x -> {
                        PolicyClaimDocument matched = policyClaimDocuments.stream()
                                .filter(y -> y.getCompanyClaim().getClaimType().equals(x.getClaimType()) && y.getDocumentType().equals(x.getDocumentCode()))
                                .findAny()
                                .orElse(null);

                        if (matched != null) {
                            x.setCondition(matched.isCondition());
                        } else {
                            x.setCondition(false);
                        }

                        return x;
                    })
                    .collect(Collectors.toList());

            return claimDocumentDtos;
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<EformDto> getListEform(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_eform",
                conditions,
                "eform_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<EformDto>>> call = portalClaim.getListEform(request);
            Response<PortalClaimBaseResponse<List<EformDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public ClaimTransactionResponseDto createClaimAuto(
            PortalClaimBaseRequest request,
            Response<PortalClaimBaseResponse<ClaimTransactionResponseDto>> response,
            ClaimTransactionDto claimTransactionDto,
            CompanyPolicy companyPolicy
    ) {
            // 2. Get CompanyClaims by policy id ✅
            Iterable<CompanyClaim> companyClaims = companyClaimRepository.findByCompanyPolicyId(companyPolicy.getId());
            // 3. Filter CompanyClaims by claimType ✅
            CompanyClaim companyClaim = StreamSupport.stream(companyClaims.spliterator(), false)
                    .filter(x -> x.getClaimType().equals(claimTransactionDto.getClaimTypeCode()))
                    .findAny()
                    .orElse(new CompanyClaim());
            // 3. Get Policy Claim auto
            Iterable<PolicyClaimAuto> policyClaimAutos =
                    policyClaimAutoRepository.findByCompanyClaimIdAndClaimTypeNotInOrderByClaimTypeAsc(companyClaim.getId(), claimTransactionDto.getClaimTypeCode());

            // 1. Check if Claim status from response API == 'Draft'
            if (response.body().getData().getPartner_claim_status().equals("SC001")) {
                if (companyClaim.isExpired()) {
                    Date currentDate = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(currentDate);
                    c.add(Calendar.DATE, companyClaim.getExpiredDays());

                    // 2. Save claim_reg_no to check expired time
                    ClaimToExpired claimToExpired = new ClaimToExpired(
                        null,
                        response.body().getData().getRegister_number(),
                        ClaimToExpiredStatus.Active,
                        c.getTime()
                    );

                    claimToExpiredRepository.save(claimToExpired);
                }
            }

            // Tarra ✨
            // 4. Check CompanyClaim's `isTriggered` recursively
            Map<String, List<PolicyClaimDocument>> claimTypesTrigger = getPolicyClaimAuto(companyClaim, companyClaims, policyClaimAutos, new HashMap<>());

            if (!claimTypesTrigger.isEmpty() && !response.body().getData().getPartner_claim_status().equals("SC001")) {
                List<ClaimTransactionAutoGeneratedDto> claimTransactionAutoGeneratedDtos = claimTypesTrigger
                    .entrySet()
                    .stream()
                    .map(x -> {
                        Set<ClaimTransactionDocumentsDto> policyClaimDocumentDtos = StreamSupport.stream(x.getValue().spliterator(), false)
                            .map(y -> {
                                if (y.isCopyParent()) {
                                    ClaimTransactionDocumentsDto found = StreamSupport.stream(claimTransactionDto.getDocuments().spliterator(), false)
                                        .filter(z -> z.getDocumentCode().equals(y.getDocumentType()))
                                        .findAny()
                                        .orElse(null);

                                    if (null != found) return found;
                                }

                                return new ClaimTransactionDocumentsDto("", y.getDocumentType());
                            }).collect(Collectors.toSet());

                        claimTransactionDto.setRemarks("Terbentuk secara otomatis atas pengajuan klaim no " + response.body().getData().getRegister_number());
                        claimTransactionDto.setClaimStatusCode("SC001");
                        claimTransactionDto.setClaimTypeCode(x.getKey());
                        claimTransactionDto.setDocuments(policyClaimDocumentDtos);

                        request.setValues(claimTransactionDto);

                        try {
                            Response<PortalClaimBaseResponse<ClaimTransactionResponseDto>> res = portalClaim.createClaimTransaction(request).execute();
                            ClaimTransactionAutoGeneratedDto claimTransactionAutoGeneratedDto = new ClaimTransactionAutoGeneratedDto(
                                res.body().getData().getRegister_number(),
                                x.getKey()
                            );

                            return claimTransactionAutoGeneratedDto;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return null;
                    }).collect(Collectors.toList());

                response.body().getData().setClaimAutoGenerated(claimTransactionAutoGeneratedDtos);
            }

            return response.body().getData();
    }

    public ClaimTransactionResponseDto createClaimTransaction(ClaimTransactionDto claimTransactionDto)
            throws IOException, ResourceNotFoundException {
        PortalClaimBaseRequest request = new PortalClaimBaseRequest<ClaimTransactionDto>(
                portalUsername,
                portalPassword,
                portalAppCode,
                "new_transaction",
                claimTransactionDto
        );

        try {
            Call<PortalClaimBaseResponse<ClaimTransactionResponseDto>> call = portalClaim.createClaimTransaction(request);
            Response<PortalClaimBaseResponse<ClaimTransactionResponseDto>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return new ClaimTransactionResponseDto();
            }

            if (!response.body().getData().getResult().equals("failed")) {
                // 1. Get ClaimPolicy's id by policyNo ✅
                CompanyPolicy companyPolicy = companyPolicyRepository.findByPolicyNo(claimTransactionDto.getPolicyNo())
                        .orElse(new CompanyPolicy());
                ClaimTransactionResponseDto updatedResponse = createClaimAuto(
                        request,
                        response,
                        claimTransactionDto,
                        companyPolicy
                );

                return updatedResponse;
            }

            return response.body().getData();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Error gan!", e);
        }
    }

    public Map<String, List<PolicyClaimDocument>> getPolicyClaimAuto(
        CompanyClaim companyClaim,
        Iterable<CompanyClaim> companyClaims,
        Iterable<PolicyClaimAuto> policyClaimAutos,
        Map<String, List<PolicyClaimDocument>> acc
    ) {
        Long policyClaimAuthCount = StreamSupport.stream(policyClaimAutos.spliterator(), false).count();

        if (null != companyClaim && policyClaimAuthCount >= 1) {
                CompanyClaim nextCompanyClaim = StreamSupport.stream(policyClaimAutos.spliterator(), false)
                    .map(x -> {
                        CompanyClaim found = StreamSupport.stream(companyClaims.spliterator(), false)
                                .filter(y -> y.getClaimType().equals(x.getClaimType()))
                                .findAny()
                                .orElse(null);

                        return found;
                    }).findAny().orElse(null);

                if (null != nextCompanyClaim) {
                    List<PolicyClaimDocument> policyClaimDocuments = policyClaimDocumentRepository.findByCompanyClaimClaimTypeOrderByDocumentTypeAsc(nextCompanyClaim.getClaimType());

                    acc.put(nextCompanyClaim.getClaimType(), policyClaimDocuments);
                }

                List<PolicyClaimAuto> nextPolicyClaimAutos = StreamSupport.stream(policyClaimAutos.spliterator(), true)
                    .skip(1)
                    .collect(Collectors.toList());

                return getPolicyClaimAuto(
                    nextCompanyClaim,
                    companyClaims,
                    nextPolicyClaimAutos,
                    acc
                );
        }

        return acc;
    }

    public List<EformDocumentDto> getListEformDocument(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_eform_document",
                conditions,
                "eform_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<EformDocumentDto>>> call = portalClaim.getListEformDocument(request);
            Response<PortalClaimBaseResponse<List<EformDocumentDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public List<DocumentDto> getListDocument(Long claimType, Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        List<PolicyClaimDocument> policyClaimDocuments = policyClaimDocumentRepository.findByCompanyClaimId(claimType);

        params.forEach((c, v) -> {
            if (claimType != null) {
                String documentIds = StreamSupport.stream(policyClaimDocuments.spliterator(), false)
                        .map(pcd -> pcd.getDocumentType())
                        .collect(Collectors.joining(","));
                String value = documentIds != "" ? documentIds : "0";

                conditions.add(new Condition("document_code", "in", value, "AND"));
                return;
            }

            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "list_document",
                conditions,
                "document_id"
        );

        try {
            Call<PortalClaimBaseResponse<List<DocumentDto>>> call = portalClaim.getListDocument(request);
            Response<PortalClaimBaseResponse<List<DocumentDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                return Arrays.asList();
            }

            List<DocumentDto> withCondition = IntStream.range(0, response.body().getData().size())
                    .mapToObj(i -> {
                        boolean condition = policyClaimDocuments != null || policyClaimDocuments.get(i) != null && policyClaimDocuments.get(i).isCondition();
                        response.body().getData().get(i)
                                .setCondition(condition);

                        return response.body().getData().get(i);
                    })
                    .collect(Collectors.toList());

            return withCondition;
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public DocumentFileDto getDocumentFile(Map<String, String> params) throws IOException {
        List<Condition> conditions = new ArrayList<Condition>();
        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "view_document",
                conditions,
                ""
        );

        try {
            Call<PortalClaimBaseResponse<DocumentFileDto>> call = portalClaim.getDocument(request);
            Response<PortalClaimBaseResponse<DocumentFileDto>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                DocumentFileDto documentFileDto = new DocumentFileDto();

                return documentFileDto;
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public UpdatedDocumentFileDto updateDocumentFile(
            String transactionDocumentFileId,
            Map<String, String> values
    ) throws IOException {
        List<Condition> condition = new ArrayList<>();
        condition.add(new Condition(
            "transaction_document_file_id",
            "equal",
            transactionDocumentFileId,
            "AND"
        ));

        PortalClaimBaseRequest request = new PortalClaimBaseRequest<Map<String, String>>(
            portalUsername,
            portalPassword,
            portalAppCode,
            "update_document_file",
            condition,
            values
        );

        try {
            Call<PortalClaimBaseResponse<UpdatedDocumentFileDto>> call = portalClaim.updateDocumentFile(request);
            Response<PortalClaimBaseResponse<UpdatedDocumentFileDto>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                UpdatedDocumentFileDto updatedDocumentFileDto = new UpdatedDocumentFileDto();

                return updatedDocumentFileDto;
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public UpdatedClaimDto submitClaim(
            SubmitCancelClaimDto body
    ) throws IOException, IllegalAccessException, ResourceNotFoundException {
        List<Condition> conditions = new ArrayList<>();

        for (Field field : body.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String column = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
            Object value = field.get(body);

            Condition condition = new Condition(column, "equal", value.toString(), "AND") ;
            conditions.add(condition);
        }

        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "submit_transaction",
                conditions,
                ""
        );

        try {
            List<ClaimPartnerDto> claimPartnerDtos = getListClaimPartner(new HashMap() {{
                put("transaction_id", body.getTransactionId());
            }});

            Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> call = portalClaim.submitClaim(request);
            Response<PortalClaimBaseResponse<List<UpdatedClaimDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                UpdatedClaimDto updatedClaimDto = new UpdatedClaimDto();

                return updatedClaimDto;
            }

            if (
                    response.body().getData().get(0).getStatus().intValue() == 0
                    || !claimPartnerDtos.get(0).getClaimStatusCode().equals("SC001")
            ) {
                return response.body().getData().get(0);
            }

            CompanyPolicy companyPolicy = companyPolicyRepository.findByPolicyNo(claimPartnerDtos.get(0).getPolicyNo())
                    .orElse(new CompanyPolicy());
            List<ClaimDocumentDto> claimDocumentDtos = getListClaimDocument(new HashMap() {{
                put("transaction_id", body.getTransactionId());
            }});

            Set<ClaimTransactionDocumentsDto> claimTransactionDocumentsDto = claimDocumentDtos.stream()
                .map(x -> {
                    try {
                        List<ClaimDocumentFileDto> claimDocumentFileDtos = getListClaimDocumentFile(new HashMap() {{
                            put("transaction_document_id", x.getTransactionDocumentId().toString());
                        }});

                        if (claimDocumentFileDtos.size() > 0 && claimDocumentFileDtos.get(0).getTransactionId() != null) {
                            return new ClaimTransactionDocumentsDto(
                                    claimDocumentFileDtos.get(0).getDocumentName(),
                                    x.getDocumentCode()
                            );
                        }
                    } catch (IOException e) {
                        return new ClaimTransactionDocumentsDto(
                                "",
                                x.getDocumentCode()
                        );
                    }

                    return new ClaimTransactionDocumentsDto(
                            "",
                            x.getDocumentCode()
                    );
                }).collect(Collectors.toSet());

            ClaimTransactionDto claimTransactionDto = new ClaimTransactionDto(
                claimPartnerDtos.get(0).getSourceApp(),
                claimPartnerDtos.get(0).getEformId(),
                claimPartnerDtos.get(0).getParticipant(),
                claimPartnerDtos.get(0).getParticipantNo(),
                claimPartnerDtos.get(0).getPhoneNo1(),
                claimPartnerDtos.get(0).getEmailAddress(),
                claimPartnerDtos.get(0).getVirtualAccount(),
                claimPartnerDtos.get(0).getPartner(),
                claimPartnerDtos.get(0).getIdUserPartner(),
                claimPartnerDtos.get(0).getIdMemberPartner(),
                claimPartnerDtos.get(0).getPolicyNo(),
                claimPartnerDtos.get(0).getProduct(),
                claimPartnerDtos.get(0).getStartDate(),
                claimPartnerDtos.get(0).getFinishDate(),
                "",
                claimPartnerDtos.get(0).getIdCurrencyPolicy(),
                claimPartnerDtos.get(0).getSumAssured(),
                claimPartnerDtos.get(0).getEventDate(),
                claimPartnerDtos.get(0).getClaimCurrencyCode(),
                claimPartnerDtos.get(0).getClaimAmount(),
                claimPartnerDtos.get(0).getClaimTypeCode(),
                claimPartnerDtos.get(0).getIllnessCode(),
                claimPartnerDtos.get(0).getIllnessText(),
                claimPartnerDtos.get(0).getClaimantIsInsured().toString(),
                claimPartnerDtos.get(0).getClaimantName(),
                claimPartnerDtos.get(0).getClaimantRelationInsuredCode(),
                claimPartnerDtos.get(0).getClaimantPhoneNo(),
                claimPartnerDtos.get(0).getClaimantEmailAddress(),
                claimPartnerDtos.get(0).getBeneficiery(),
                claimPartnerDtos.get(0).getBeneficieryRelationInsuredCode(),
                claimPartnerDtos.get(0).getBeneficieryKtpNo(),
                claimPartnerDtos.get(0).getBeneficieryFamilyCardNo(),
                claimPartnerDtos.get(0).getPartnerAccountNo(),
                claimPartnerDtos.get(0).getPartnerBank(),
                claimPartnerDtos.get(0).getPartnerBankText(),
                claimPartnerDtos.get(0).getPartnerCurrency(),
                claimPartnerDtos.get(0).getPartnerAccountName(),
                claimPartnerDtos.get(0).getClaimStatusCode(),
                claimPartnerDtos.get(0).getIdUserPartner(),
                claimTransactionDocumentsDto
            );

            // 2. Get CompanyClaims by policy id ✅
            Iterable<CompanyClaim> companyClaims = companyClaimRepository.findByCompanyPolicyId(companyPolicy.getId());
            // 3. Filter CompanyClaims by claimType ✅
            CompanyClaim companyClaim = StreamSupport.stream(companyClaims.spliterator(), false)
                    .filter(x -> x.getClaimType().equals(claimTransactionDto.getClaimTypeCode()))
                    .findAny()
                    .orElse(new CompanyClaim());
            // 3. Get Policy Claim auto
            Iterable<PolicyClaimAuto> policyClaimAutos =
                    policyClaimAutoRepository.findByCompanyClaimIdAndClaimTypeNotInOrderByClaimTypeAsc(companyClaim.getId(), claimTransactionDto.getClaimTypeCode());

            // Tarra ✨
            // 4. Check CompanyClaim's `isTriggered` recursively
            Map<String, List<PolicyClaimDocument>> claimTypesTrigger = getPolicyClaimAuto(companyClaim, companyClaims, policyClaimAutos, new HashMap<>());

            PortalClaimBaseRequest requestNewTransaction = new PortalClaimBaseRequest<>(
                portalUsername,
                portalPassword,
                portalAppCode,
                "new_transaction",
                new ClaimTransactionDto()
            );

            if (!claimTypesTrigger.isEmpty()) {
                List<ClaimTransactionAutoGeneratedDto> claimTransactionAutoGeneratedDtos = claimTypesTrigger
                        .entrySet()
                        .stream()
                        .map(x -> {
                            Set<ClaimTransactionDocumentsDto> policyClaimDocumentDtos = StreamSupport.stream(x.getValue().spliterator(), false)
                                    .map(y -> {
                                        if (y.isCopyParent()) {
                                            ClaimTransactionDocumentsDto found = StreamSupport.stream(claimTransactionDto.getDocuments().spliterator(), false)
                                                    .filter(z -> z.getDocumentCode().equals(y.getDocumentType()))
                                                    .findAny()
                                                    .orElse(null);

                                            if (null != found) return found;
                                        }

                                        return new ClaimTransactionDocumentsDto("", y.getDocumentType());
                                    }).collect(Collectors.toSet());

                            claimTransactionDto.setRemarks("Terbentuk secara otomatis atas pengajuan klaim no " + claimPartnerDtos.get(0).getClaimRegNo());
                            claimTransactionDto.setClaimStatusCode("SC001");
                            claimTransactionDto.setClaimTypeCode(x.getKey());
                            claimTransactionDto.setDocuments(policyClaimDocumentDtos);

                            requestNewTransaction.setValues(claimTransactionDto);

                            try {
                                Response<PortalClaimBaseResponse<ClaimTransactionResponseDto>> res = portalClaim.createClaimTransaction(requestNewTransaction).execute();

                                if (res.body().getData().getResult().equals("failed")) {
                                    return null;
                                }

                                ClaimTransactionAutoGeneratedDto claimTransactionAutoGeneratedDto = new ClaimTransactionAutoGeneratedDto(
                                        res.body().getData().getRegister_number(),
                                        x.getKey()
                                );

                                return claimTransactionAutoGeneratedDto;
                            } catch (IOException e) {
                                e.printStackTrace();
                                return null;
                            }
                        }).collect(Collectors.toList());

                response.body().getData().get(0).setClaimAutoGenerated(claimTransactionAutoGeneratedDtos);
            }

            return response.body().getData().get(0);
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public UpdatedClaimDto saveClaim(
            SaveClaimDto body
    ) throws IOException, IllegalAccessException {
        List<Condition> conditions = new ArrayList<>();

        for (Field field : body.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String column = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
            Object value;

            if (null != field.get(body)) {
                value = field.get(body);
                Condition condition = new Condition(column, "equal", value.toString(), "AND");
                conditions.add(condition);
            }
        }

        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "save_transaction",
                conditions,
                ""
        );

        try {
            Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> call = portalClaim.saveClaim(request);
            Response<PortalClaimBaseResponse<List<UpdatedClaimDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                UpdatedClaimDto updatedClaimDto = new UpdatedClaimDto();

                return updatedClaimDto;
            }

            return response.body().getData().get(0);
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public UpdatedClaimDto cancelClaim(
            CancelClaimDto body
    ) throws IOException, IllegalAccessException {
        List<Condition> conditions = new ArrayList<>();

        for (Field field : body.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String column = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
            Object value = field.get(body);

            Condition condition = new Condition(column, "equal", value.toString(), "AND") ;
            conditions.add(condition);
        }

        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "cancel_transaction",
                conditions,
                ""
        );

        try {
            Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> call = portalClaim.cancelClaim(request);
            Response<PortalClaimBaseResponse<List<UpdatedClaimDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null) {
                UpdatedClaimDto updatedClaimDto = new UpdatedClaimDto();

                return updatedClaimDto;
            }

            return response.body().getData().get(0);
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public AddedDocumentFileDto addDocumentFile(AddDocumentFileDto addDocumentFileDto) throws IOException {
        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "add_document",
                addDocumentFileDto
        );

        try {
            Call<PortalClaimBaseResponse<AddedDocumentFileDto>> call = portalClaim.addDocumentFile(request);
            Response<PortalClaimBaseResponse<AddedDocumentFileDto>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body().getData() == null && !response.body().getError_code().equals("200")) {
                AddedDocumentFileDto addedDocumentFileDto = new AddedDocumentFileDto();

                return addedDocumentFileDto;
            }

            return response.body().getData();
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }

    public UpdatedClaimDto expiredClaim(Map<String, String> params)
            throws IOException, IllegalAccessException {
        List<Condition> conditions = new ArrayList<>();

        params.forEach((c, v) -> {
            conditions.add(new Condition(c, "equal", v, "AND"));
        });

        PortalClaimBaseRequest request = new PortalClaimBaseRequest(
                portalUsername,
                portalPassword,
                portalAppCode,
                "expired_transaction",
                conditions,
                ""
        );

        try {
            Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> call = portalClaim.expiredClaim(request);
            Response<PortalClaimBaseResponse<List<UpdatedClaimDto>>> response = call.execute();

            if (!response.isSuccessful()) {
                throw new IOException(response.errorBody() != null
                        ? response.errorBody().string()
                        : "Unknown error");
            }

            if (response.body() == null || response.body().getData() == null) {
                return null;
            }

            return response.body().getData().get(0);
        } catch (IOException e) {
            throw new IOException("Error gan!", e);
        }
    }
}
