FROM maven:3.6.1-jdk-11-slim AS MAVEN_TOOL_CHAIN
ARG BUILD_WAR
COPY pom.xml /tmp/
COPY src /tmp/src/
COPY script /tmp/script
WORKDIR /tmp/
RUN chmod +x ./script/check-build-war.sh
RUN ./script/check-build-war.sh pom.xml
RUN mvn dependency:go-offline
RUN mvn package -DskipTests

FROM openjdk:11-jdk-slim AS jdk
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/portal-partner-0.0.1-SNAPSHOT.jar /
EXPOSE 10000
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/portal-partner-0.0.1-SNAPSHOT.jar"]
